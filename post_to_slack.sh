#!/bin/bash

# post_to_slack.sh

# Make sure to set the SLACK_WEBHOOK_URL environment variable before running this script

# Check if the SLACK_WEBHOOK_URL variable is set
if [ -z "$SLACK_WEBHOOK_URL" ]; then
    echo "Error: Please set the SLACK_WEBHOOK_URL environment variable before running this script."
    exit 1
fi

# Run the command
arxiv-post slack \
    --slack_webhook_url $SLACK_WEBHOOK_URL \
    --debug \
    --target_lang en \
    --start_date '2 days ago in UTC' \
    --end_date 'now in UTC' \
    --categories quant-ph
